//
//  AppDelegate.swift
//  Hmap
//
//  Created by Katherine Zambrano on 2017-06-25.
//  Copyright © 2017 The University of Western Ontario. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        //let allSites = uploadSampleDataSites()
        var siteDictionary = uploadSampleDataSites()
        print("Sample data sites uploaded")
        var site = siteDictionary["H22"]!
        print("After uploaded site coord: \(site.coord1!)")

        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    func uploadSampleDataSites()->[String: Site]{
        var finalArray = [String: Site]()
        let url = Bundle.main.url(forResource: "rooms-sjh-app2", withExtension: "JSON")
        
        do{
            let data = try Data(contentsOf: url!)
            let jsonResult = try JSONSerialization.jsonObject(with: data) as? [String: Any]
            let sitesArray = jsonResult?["rooms"] as? [[String: Any]]
            for site in sitesArray! {
                let name = site["name"] as! String
                print("siteName:\(name)")
                let coord1 = site["coord1"] as! String
                print("coord1:\(coord1)")
                let coord2 = site["coord2"] as! String
                print("coord2:\(coord2)")
                let left = site["left"] as! String
                print("left:\(left)")
                let right = site["right"] as! String
                print("right:\(right)")
                let up = site["up"] as! String
                print("up:\(up)")
                let down = site["down"] as! String
                print("down:\(down)")
                let access_point = site["access_point"] as! String
                print("access point:\(access_point)")
                
                
                let sites = Site(name: name, coord1: coord1, coord2: coord2, left: left, right:right, up: up, down: down, access_point: access_point)
                finalArray[name] = sites
            }
            /*for student in finalArray{
                print(student.userName!)
            }*/
        }catch{
            print("error getting sample data")
        }
        
        return finalArray
    }


}

