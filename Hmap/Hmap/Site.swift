//
//  File.swift
//  Hmap
//
//  Created by Katherine Zambrano on 2017-08-03.
//  Copyright © 2017 The University of Western Ontario. All rights reserved.
//

import Foundation
class Site{

    var name: String!
    var coord1: Int!
    var coord2: Int!
    var coordinates: coordinates! //Vectors used as coordinates
    //var floor: Bool!
    var neighbors: neighbors!
    var left, right, up, down, access_point: String!
    var popped, pushed : Bool!
    init(name: String, coord1: String, coord2: String, left: String, right: String, up: String, down: String, access_point: String){
        self.name = name
        self.coord1 = Int(coord1)
        self.coord2 = Int(coord2)
        self.left = left
        self.right = right
        self.up = up
        self.down = down
        self.access_point = access_point
        self.popped = false
        self.pushed = false
    
    }


}
struct coordinates{
    var coord1: Int!
    var coord2: Int!
}
struct neighbors{
    var left: String!
    var right: String!
    var up: String!
    var down: String!
    
}
