//
//  mapNav.swift
//  Hmap
//
//  Created by Katherine Zambrano on 2017-08-03.
//  Copyright © 2017 The University of Western Ontario. All rights reserved.
//

import Foundation
import UIKit
import EventKit

class MapNav: UIViewController, UIScrollViewDelegate, UITextFieldDelegate{
    var sites = [String: Site]()
    var destination = goTo()
    var location = goTo()
    var fastesRoute = Array<String>()
    var finalRouteFound = false
    var fastestRouteFound1 = false
    var fastestRouteFound2 = false
    var fastestRouteFound3 = false
    var fastestRouteFound4 = false


    var up, down, right, left: Bool!
    var timer : Timer?
    var stack1 = Array<Site>()
    var stack2 = Array<Site>()
    var stack3 = Array<Site>()
    var stack4 = Array<Site>()
    var finalStack = Array<Site>()
    /* var numberOfSteps, counts steps that it takes to get to the destination
       var counter, used to animate the blue dots appearing in the map, it increases with the timer until reacher the same number as numberOfSteps
     */
    var numberOfsteps = 0, numberOfsteps1 = 0,numberOfsteps2 = 0,numberOfsteps3 = 0,numberOfsteps4 = 0, neighCounter = 0, neighCounter2 = 0,neighCounter3 = 0,neighCounter4 = 0, counter = 0

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var H1: UIButton!
    @IBOutlet weak var H2: UIButton!
    @IBOutlet weak var H3: UIButton!
    @IBOutlet weak var H4: UIButton!
    @IBOutlet weak var H5: UIButton!
    @IBOutlet weak var H6: UIButton!
    @IBOutlet weak var H7: UIButton!
    @IBOutlet weak var H8: UIButton!
    @IBOutlet weak var H9: UIButton!
    @IBOutlet weak var H10: UIButton!
    @IBOutlet weak var H11: UIButton!
    @IBOutlet weak var H12: UIButton!
    @IBOutlet weak var H13: UIButton!
    @IBOutlet weak var H14: UIButton!
    @IBOutlet weak var H15: UIButton!
    @IBOutlet weak var H16: UIButton!
    @IBOutlet weak var H17: UIButton!
    @IBOutlet weak var H18: UIButton!
    @IBOutlet weak var H19: UIButton!
    @IBOutlet weak var H20: UIButton!
    @IBOutlet weak var H21: UIButton!
    @IBOutlet weak var H22: UIButton!
    @IBOutlet weak var H23: UIButton!
    @IBOutlet weak var H24: UIButton!
    @IBOutlet weak var H25: UIButton!
    @IBOutlet weak var H26: UIButton!
    @IBOutlet weak var H27: UIButton!
    @IBOutlet weak var H28: UIButton!
    @IBOutlet weak var H29: UIButton!
    @IBOutlet weak var H30: UIButton!
    @IBOutlet weak var H31: UIButton!
    @IBOutlet weak var H32: UIButton!
    @IBOutlet weak var H33: UIButton!
    @IBOutlet weak var H88: UIButton!
    @IBOutlet weak var H34: UIButton!
    @IBOutlet weak var H35: UIButton!
    @IBOutlet weak var H36: UIButton!
    @IBOutlet weak var H37: UIButton!
    @IBOutlet weak var H38: UIButton!
    @IBOutlet weak var H39: UIButton!
    @IBOutlet weak var H40: UIButton!
    @IBOutlet weak var H41: UIButton!
    @IBOutlet weak var H42: UIButton!
    @IBOutlet weak var H43: UIButton!
    @IBOutlet weak var H44: UIButton!
    @IBOutlet weak var H45: UIButton!
    @IBOutlet weak var H46: UIButton!
    @IBOutlet weak var H47: UIButton!
    @IBOutlet weak var H48: UIButton!
    @IBOutlet weak var H49: UIButton!
    @IBOutlet weak var H50: UIButton!
    @IBOutlet weak var H51: UIButton!
    @IBOutlet weak var H52: UIButton!
    @IBOutlet weak var H53: UIButton!
    @IBOutlet weak var H54: UIButton!
    @IBOutlet weak var H55: UIButton!
    @IBOutlet weak var H56: UIButton!
    @IBOutlet weak var H57: UIButton!
    @IBOutlet weak var H58: UIButton!
    @IBOutlet weak var H59: UIButton!
    @IBOutlet weak var H60: UIButton!
    @IBOutlet weak var H61: UIButton!
    @IBOutlet weak var H62: UIButton!
    @IBOutlet weak var H63: UIButton!
    @IBOutlet weak var H64: UIButton!
    @IBOutlet weak var H65: UIButton!
    @IBOutlet weak var H66: UIButton!
    @IBOutlet weak var H67: UIButton!
    @IBOutlet weak var H68: UIButton!
    @IBOutlet weak var H69: UIButton!
    @IBOutlet weak var H70: UIButton!
    @IBOutlet weak var H71: UIButton!
    @IBOutlet weak var H72: UIButton!
    @IBOutlet weak var H73: UIButton!
    @IBOutlet weak var H74: UIButton!
    @IBOutlet weak var H75: UIButton!
    @IBOutlet weak var H76: UIButton!
    @IBOutlet weak var H77: UIButton!
    @IBOutlet weak var H78: UIButton!
    @IBOutlet weak var H79: UIButton!
    @IBOutlet weak var H80: UIButton!
    @IBOutlet weak var H81: UIButton!
    @IBOutlet weak var H82: UIButton!
    @IBOutlet weak var H83: UIButton!
    @IBOutlet weak var H84: UIButton!
    @IBOutlet weak var H85: UIButton!
    @IBOutlet weak var H86: UIButton!
    @IBOutlet weak var H87: UIButton!
    
    @IBAction func setLocationSelectedFalse(_ sender: Any) {
        location.selected = false
    }
 
    @IBAction func getNameLocationDestination(_ sender: UIButton) {
        let name = sender.titleLabel!.text!
        print("name: \(name)")
        
        if(location.selected == false){location.selected = true
            destination.selected = false
            from.text! = name
        }else{
            destination.selected = true
            finalDestination.text! = name
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sites = AppDelegate().uploadSampleDataSites()
        let newSite = sites["H22"]
        
        print("site coord 1:\(newSite!.coord1!)")
        
        
        
        
        if (fastestRouteFound1 || fastestRouteFound2 || fastestRouteFound3 || fastestRouteFound4){
            
            if(stack1.count <= stack2.count && stack1.count <= stack3.count && stack1.count <= stack4.count){
                finalStack = stack1
                print("final stack 1")
                finalRouteFound = true
            }else if(stack2.count <= stack1.count && stack2.count <= stack3.count && stack2.count <= stack4.count){
                finalStack = stack2
                print("final stack 2")
                finalRouteFound = true
            }else if(stack3.count <= stack1.count && stack3.count <= stack2.count && stack3.count <= stack4.count){
                finalStack = stack3
                print("final stack 3")
                finalRouteFound = true
            }else if(stack4.count <= stack1.count && stack4.count <= stack3.count && stack4.count <= stack2.count){
                finalStack = stack4
                print("final stack 4")
                finalRouteFound = true
            }
            
            self.startPainting()
        }
        if(counter == numberOfsteps){
            
            print("Number of steps and counter are equal")
            
        }
        
        /*setting up scroll view to zoom in*/
        self.scrollView.minimumZoomScale = 1.0
        self.scrollView.maximumZoomScale = 6.0
        self.scrollView.delegate = self
        
        //this setup helps the keyboard hide after pressing done or return
        self.from.delegate = self
        self.finalDestination.delegate = self
        location.selected = false
        
    }
    
    @IBAction func takeMe(_ sender: UIButton) {
        location.reset()
        destination.reset()
        
        fastestRouteFound1 = false
        fastestRouteFound2 = false
        fastestRouteFound3 = false
        fastestRouteFound4 = false
        
        let temp_location = sites[from.text!]
        
        stack1.removeAll()
        stack2.removeAll()
        stack3.removeAll()
        stack4.removeAll()
        
        if(temp_location!.access_point != nil){
            location.name = temp_location!.access_point!
            print("location name: \(location.name)")
 
            
        }else{
            location.name = temp_location!.name
            print("location name: \(location.name)")

        }

        let temp_destination = sites[finalDestination.text!]
        destination.name = temp_destination!.access_point!
        print("destiation name: \(destination.name)")

      
        
        self.findWay1(from: location.name, to: destination.name)
        self.findWay2(from: location.name, to: destination.name)
        self.findWay3(from: location.name, to: destination.name)
        self.findWay4(from: location.name, to: destination.name)
        
        for a in fastesRoute{
            print("fastes route: \(a)")
        }
        
        for site in stack1{
            print("stack 1 site name: \(site.name!)")
        }
        for site in stack2{
            print("stack 2 site name: \(site.name!)")
        }
        for site in stack3{
            print("stack 3 site name: \(site.name!)")
        }
        for site in stack4{
            print("stack 4 site name: \(site.name!)")
        }
        
        print("steps stack 1: \(stack1.count)")
        print("steps stack 2: \(stack2.count)")
        print("steps stack 3: \(stack3.count)")
        print("steps stack 4: \(stack4.count)")
        
        let blueDot = UIImage(named: "white")
        H1.setImage(blueDot, for: .normal)
        H2.setImage(blueDot, for: .normal)
        H3.setImage(blueDot, for: .normal)
        H4.setImage(blueDot, for: .normal)
        H5.setImage(blueDot, for: .normal)
        H6.setImage(blueDot, for: .normal)
        H7.setImage(blueDot, for: .normal)
        H8.setImage(blueDot, for: .normal)
        H9.setImage(blueDot, for: .normal)
        H10.setImage(blueDot, for: .normal)
        H11.setImage(blueDot, for: .normal)
        H12.setImage(blueDot, for: .normal)
        H13.setImage(blueDot, for: .normal)
        H14.setImage(blueDot, for: .normal)
        H15.setImage(blueDot, for: .normal)
        H16.setImage(blueDot, for: .normal)
        H17.setImage(blueDot, for: .normal)
        H18.setImage(blueDot, for: .normal)
        H19.setImage(blueDot, for: .normal)
        H20.setImage(blueDot, for: .normal)
        H21.setImage(blueDot, for: .normal)
        H22.setImage(blueDot, for: .normal)
        H23.setImage(blueDot, for: .normal)
        H24.setImage(blueDot, for: .normal)
        H25.setImage(blueDot, for: .normal)
        H26.setImage(blueDot, for: .normal)
        H27.setImage(blueDot, for: .normal)
        H28.setImage(blueDot, for: .normal)
        H29.setImage(blueDot, for: .normal)
        H30.setImage(blueDot, for: .normal)
        H31.setImage(blueDot, for: .normal)
        H32.setImage(blueDot, for: .normal)
        H33.setImage(blueDot, for: .normal)
        H34.setImage(blueDot, for: .normal)
        H35.setImage(blueDot, for: .normal)
        H36.setImage(blueDot, for: .normal)
        H37.setImage(blueDot, for: .normal)
        H38.setImage(blueDot, for: .normal)
        H39.setImage(blueDot, for: .normal)
        H40.setImage(blueDot, for: .normal)
        H41.setImage(blueDot, for: .normal)
        H42.setImage(blueDot, for: .normal)
        H43.setImage(blueDot, for: .normal)
        H44.setImage(blueDot, for: .normal)
        H45.setImage(blueDot, for: .normal)
        H46.setImage(blueDot, for: .normal)
        H47.setImage(blueDot, for: .normal)
        H48.setImage(blueDot, for: .normal)
        H49.setImage(blueDot, for: .normal)
        H50.setImage(blueDot, for: .normal)
        H51.setImage(blueDot, for: .normal)
        H52.setImage(blueDot, for: .normal)
        H53.setImage(blueDot, for: .normal)
        H54.setImage(blueDot, for: .normal)
        H55.setImage(blueDot, for: .normal)
        H56.setImage(blueDot, for: .normal)
        H57.setImage(blueDot, for: .normal)
        H58.setImage(blueDot, for: .normal)
        H59.setImage(blueDot, for: .normal)
        H60.setImage(blueDot, for: .normal)
        H61.setImage(blueDot, for: .normal)
        H62.setImage(blueDot, for: .normal)
        H63.setImage(blueDot, for: .normal)
        H64.setImage(blueDot, for: .normal)
        H65.setImage(blueDot, for: .normal)
        H66.setImage(blueDot, for: .normal)
        H67.setImage(blueDot, for: .normal)
        H68.setImage(blueDot, for: .normal)
        H69.setImage(blueDot, for: .normal)
        H70.setImage(blueDot, for: .normal)
        H71.setImage(blueDot, for: .normal)
        H72.setImage(blueDot, for: .normal)
        H73.setImage(blueDot, for: .normal)
        H74.setImage(blueDot, for: .normal)
        H75.setImage(blueDot, for: .normal)
        H76.setImage(blueDot, for: .normal)
        H77.setImage(blueDot, for: .normal)
        H78.setImage(blueDot, for: .normal)
        H79.setImage(blueDot, for: .normal)
        H80.setImage(blueDot, for: .normal)
        H81.setImage(blueDot, for: .normal)
        H82.setImage(blueDot, for: .normal)
        H83.setImage(blueDot, for: .normal)
        H84.setImage(blueDot, for: .normal)
        H85.setImage(blueDot, for: .normal)
        H86.setImage(blueDot, for: .normal)
        H87.setImage(blueDot, for: .normal)
        H88.setImage(blueDot, for: .normal)
        
        
}
    
    @IBOutlet weak var finalDestination: UITextField!
    @IBOutlet weak var from: UITextField!

    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.mainView
    }
    
    
    
    func findWay1(from: String, to: String){
        var currentSite: Site!
        var destinationSite: Site!
        currentSite = sites[from]
        destinationSite = sites[to]
        currentSite.pushed = true
        
        stack1.append(currentSite)
        
        while(!stack1.isEmpty && !fastestRouteFound1){
            
            print("fastest route is not empty and hasn't been found yet")
            currentSite = stack1.last
            if(currentSite!.name!==destinationSite!.name!){
                
                print("found destination")
                fastestRouteFound1 = true
                self.viewDidLoad()
                break
            }else{
                let neighbors = [currentSite!.left!, currentSite!.right!, currentSite!.up!, currentSite!.down! ]
                for index in 0...3{
                    
                    let newNeighbor = neighbors[index]
                    if (newNeighbor != "nil"){
                        
                        let realNeighbor = sites[newNeighbor]
                        if (!realNeighbor!.pushed && !realNeighbor!.popped){
                            realNeighbor!.pushed! = true
                            stack1.append(realNeighbor!)
                            numberOfsteps1 += 1
                            neighCounter += 1
                        }
                    }
                }
                if(currentSite.pushed && neighCounter == 0){
                    currentSite.popped = true
                    stack1.popLast()
                    print("site popped")
                }else{
                    neighCounter = 0
                }
                
                
            }
            
        }
        
        
    }
    func findWay2(from: String, to: String){
        var currentSite: Site!
        var destinationSite: Site!
        currentSite = sites[from]
        destinationSite = sites[to]
        currentSite.pushed = true
        stack2.append(currentSite)
        while(!stack2.isEmpty && !fastestRouteFound2){
            print("fastest route is not empty and hasn't been found")
            currentSite = stack2.last
            if(currentSite!.name!==destinationSite!.name!){
                
                print("found destination")
                fastestRouteFound1 = true
                self.viewDidLoad()
                break
            }else{
                let neighbors = [ currentSite!.right!, currentSite!.up!, currentSite!.down!,currentSite!.left! ]
                for index in 0...3{
                    
                    let newNeighbor = neighbors[index]
                    if (newNeighbor != "nil"){
                        
                        let realNeighbor = sites[newNeighbor]
                        if (!realNeighbor!.pushed && !realNeighbor!.popped){
                            realNeighbor!.pushed! = true
                            stack2.append(realNeighbor!)
                            numberOfsteps2 += 1
                            neighCounter2 += 1
                        }
                    }
                }
                if(currentSite.pushed && neighCounter2 == 0){
                    currentSite.popped = true
                    stack2.popLast()
                    print("site popped")
                }else{
                    neighCounter2 = 0
                }
                
                
            }
            
        }
        
        
    }
    func findWay3(from: String, to: String){
        var currentSite: Site!
        var destinationSite: Site!
        currentSite = sites[from]
        destinationSite = sites[to]
        currentSite.pushed = true
        stack3.append(currentSite)
        while(!stack3.isEmpty && !fastestRouteFound3){
            print("fastest route is not empty and hasn't been found")
            currentSite = stack3.last
            if(currentSite!.name!==destinationSite!.name!){
                
                print("found destination")
                fastestRouteFound3 = true
                self.viewDidLoad()
                break
            }else{
                let neighbors = [currentSite!.up!, currentSite!.down!,currentSite!.left!,currentSite!.right!]
                for index in 0...3{
                    
                    let newNeighbor = neighbors[index]
                    if (newNeighbor != "nil"){
                        
                        let realNeighbor = sites[newNeighbor]
                        if (!realNeighbor!.pushed && !realNeighbor!.popped){
                            realNeighbor!.pushed! = true
                            stack3.append(realNeighbor!)
                            numberOfsteps3 += 1
                            neighCounter3 += 1
                        }
                    }
                }
                if(currentSite.pushed && neighCounter3 == 0){
                    currentSite.popped = true
                    stack3.popLast()
                    print("site popped")
                }else{
                    neighCounter3 = 0
                }
                
            }
        }
    }
    
    
    func findWay4(from: String, to: String){
        var currentSite: Site!
        var destinationSite: Site!
        currentSite = sites[from]
        destinationSite = sites[to]
        currentSite.pushed = true
        stack4.append(currentSite)
        while(!stack4.isEmpty && !fastestRouteFound4){
            print("fastest route is not empty and hasn't been found")
            currentSite = stack4.last
            if(currentSite!.name!==destinationSite!.name!){
                
                print("found destination")
                fastestRouteFound4 = true
                self.viewDidLoad()
                break
            }else{
                let neighbors = [currentSite!.down!,currentSite!.left!,currentSite!.right!,currentSite!.up!]
                for index in 0...3{
                    
                    let newNeighbor = neighbors[index]
                    if (newNeighbor != "nil"){
                        
                        let realNeighbor = sites[newNeighbor]
                        if (!realNeighbor!.pushed && !realNeighbor!.popped){
                            realNeighbor!.pushed! = true
                            stack4.append(realNeighbor!)
                            numberOfsteps4 += 1
                            neighCounter4 += 1
                        }
                    }
                }
                if(currentSite.pushed && neighCounter4 == 0){
                    currentSite.popped = true
                    stack4.popLast()
                    print("site popped")
                }else{
                    neighCounter4 = 0
                }
                
                
            }
            
        }
        
        
    }
    
    func startPainting(){
        
        counter = 0
        numberOfsteps = finalStack.count
        print("number of steps\(numberOfsteps)")
        
        guard timer == nil else { return }
        timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(updateCounterAndFinishPainting), userInfo: nil, repeats: true)
        
        
    }
    
    @objc func updateCounterAndFinishPainting(){
        
        let blueDot = UIImage(named: "blue-dot.png")
        
        print("counter\(counter), number of steps inside finish painting \(numberOfsteps)")
        print("finalStack: \(finalStack)")

        let room = finalStack[counter]
        print("painting room: \(room.name!)")
        switch room.name!{
        case H1.titleLabel!.text!:
            H1.setImage(blueDot, for: .normal)
        case H2.titleLabel!.text!:
            H2.setImage(blueDot, for: .normal)
        case H3.titleLabel!.text!:
            H3.setImage(blueDot, for: .normal)
        case H4.titleLabel!.text!:
            H4.setImage(blueDot, for: .normal)
        case H5.titleLabel!.text!:
            H5.setImage(blueDot, for: .normal)
        case H6.titleLabel!.text!:
            H6.setImage(blueDot, for: .normal)
        case H7.titleLabel!.text!:
            H7.setImage(blueDot, for: .normal)
        case H8.titleLabel!.text!:
            H8.setImage(blueDot, for: .normal)
        case H9.titleLabel!.text!:
            H9.setImage(blueDot, for: .normal)
        case H10.titleLabel!.text!:
            H10.setImage(blueDot, for: .normal)
        case H11.titleLabel!.text!:
            H11.setImage(blueDot, for: .normal)
        case H12.titleLabel!.text!:
            H12.setImage(blueDot, for: .normal)
        case H13.titleLabel!.text!:
            H13.setImage(blueDot, for: .normal)
        case H14.titleLabel!.text!:
            H14.setImage(blueDot, for: .normal)
        case H15.titleLabel!.text!:
            H15.setImage(blueDot, for: .normal)
        case H16.titleLabel!.text!:
            H16.setImage(blueDot, for: .normal)
        case H17.titleLabel!.text!:
            H17.setImage(blueDot, for: .normal)
        case H18.titleLabel!.text!:
            H18.setImage(blueDot, for: .normal)
        case H19.titleLabel!.text!:
            H19.setImage(blueDot, for: .normal)
        case H20.titleLabel!.text!:
            H20.setImage(blueDot, for: .normal)
        case H21.titleLabel!.text!:
            H21.setImage(blueDot, for: .normal)
        case H22.titleLabel!.text!:
            H22.setImage(blueDot, for: .normal)
        case H23.titleLabel!.text!:
            H23.setImage(blueDot, for: .normal)
        case H24.titleLabel!.text!:
            H24.setImage(blueDot, for: .normal)
        case H25.titleLabel!.text!:
            H25.setImage(blueDot, for: .normal)
        case H26.titleLabel!.text!:
            H26.setImage(blueDot, for: .normal)
        case H27.titleLabel!.text!:
            H27.setImage(blueDot, for: .normal)
        case H28.titleLabel!.text!:
            H28.setImage(blueDot, for: .normal)
        case H29.titleLabel!.text!:
            H29.setImage(blueDot, for: .normal)
        case H30.titleLabel!.text!:
            H30.setImage(blueDot, for: .normal)
        case H31.titleLabel!.text!:
            H31.setImage(blueDot, for: .normal)
        case H32.titleLabel!.text!:
            H32.setImage(blueDot, for: .normal)
        case H33.titleLabel!.text!:
            H33.setImage(blueDot, for: .normal)
        case H34.titleLabel!.text!:
            H34.setImage(blueDot, for: .normal)
        case H35.titleLabel!.text!:
            H35.setImage(blueDot, for: .normal)
        case H36.titleLabel!.text!:
            H36.setImage(blueDot, for: .normal)
        case H37.titleLabel!.text!:
            H37.setImage(blueDot, for: .normal)
        case H38.titleLabel!.text!:
            H38.setImage(blueDot, for: .normal)
        case H39.titleLabel!.text!:
            H39.setImage(blueDot, for: .normal)
        case H40.titleLabel!.text!:
            H40.setImage(blueDot, for: .normal)
        case H41.titleLabel!.text!:
            H41.setImage(blueDot, for: .normal)
        case H42.titleLabel!.text!:
            H42.setImage(blueDot, for: .normal)
        case H43.titleLabel!.text!:
            H43.setImage(blueDot, for: .normal)
        case H44.titleLabel!.text!:
            H44.setImage(blueDot, for: .normal)
        case H45.titleLabel!.text!:
            H45.setImage(blueDot, for: .normal)
        case H46.titleLabel!.text!:
            H46.setImage(blueDot, for: .normal)
        case H47.titleLabel!.text!:
            H47.setImage(blueDot, for: .normal)
        case H48.titleLabel!.text!:
            H48.setImage(blueDot, for: .normal)
        case H49.titleLabel!.text!:
            H49.setImage(blueDot, for: .normal)
        case H50.titleLabel!.text!:
            H50.setImage(blueDot, for: .normal)
        case H51.titleLabel!.text!:
            H51.setImage(blueDot, for: .normal)
        case H52.titleLabel!.text!:
            H52.setImage(blueDot, for: .normal)
        case H53.titleLabel!.text!:
            H53.setImage(blueDot, for: .normal)
        case H54.titleLabel!.text!:
            H54.setImage(blueDot, for: .normal)
        case H55.titleLabel!.text!:
            H55.setImage(blueDot, for: .normal)
        case H56.titleLabel!.text!:
            H56.setImage(blueDot, for: .normal)
        case H57.titleLabel!.text!:
            H57.setImage(blueDot, for: .normal)
        case H58.titleLabel!.text!:
            H58.setImage(blueDot, for: .normal)
        case H59.titleLabel!.text!:
            H59.setImage(blueDot, for: .normal)
        case H60.titleLabel!.text!:
            H60.setImage(blueDot, for: .normal)
        case H61.titleLabel!.text!:
            H61.setImage(blueDot, for: .normal)
        case H62.titleLabel!.text!:
            H62.setImage(blueDot, for: .normal)
        case H63.titleLabel!.text!:
            H63.setImage(blueDot, for: .normal)
        case H64.titleLabel!.text!:
            H64.setImage(blueDot, for: .normal)
        case H65.titleLabel!.text!:
            H65.setImage(blueDot, for: .normal)
        case H66.titleLabel!.text!:
            H66.setImage(blueDot, for: .normal)
        case H67.titleLabel!.text!:
            H67.setImage(blueDot, for: .normal)
        case H68.titleLabel!.text!:
            H68.setImage(blueDot, for: .normal)
        case H69.titleLabel!.text!:
            H69.setImage(blueDot, for: .normal)
        case H70.titleLabel!.text!:
            H70.setImage(blueDot, for: .normal)
        case H71.titleLabel!.text!:
            H71.setImage(blueDot, for: .normal)
        case H72.titleLabel!.text!:
            H72.setImage(blueDot, for: .normal)
        case H73.titleLabel!.text!:
            H73.setImage(blueDot, for: .normal)
        case H74.titleLabel!.text!:
            H74.setImage(blueDot, for: .normal)
        case H75.titleLabel!.text!:
            H75.setImage(blueDot, for: .normal)
        case H76.titleLabel!.text!:
            H76.setImage(blueDot, for: .normal)
        case H77.titleLabel!.text!:
            H77.setImage(blueDot, for: .normal)
        case H78.titleLabel!.text!:
            H78.setImage(blueDot, for: .normal)
        case H79.titleLabel!.text!:
            H79.setImage(blueDot, for: .normal)
        case H80.titleLabel!.text!:
            H80.setImage(blueDot, for: .normal)
        case H81.titleLabel!.text!:
            H81.setImage(blueDot, for: .normal)
        case H82.titleLabel!.text!:
            H82.setImage(blueDot, for: .normal)
        case H83.titleLabel!.text!:
            H83.setImage(blueDot, for: .normal)
        case H84.titleLabel!.text!:
            H84.setImage(blueDot, for: .normal)
        case H85.titleLabel!.text!:
            H85.setImage(blueDot, for: .normal)
        case H86.titleLabel!.text!:
            H86.setImage(blueDot, for: .normal)
        case H87.titleLabel!.text!:
            H87.setImage(blueDot, for: .normal)
        case H88.titleLabel!.text!:
            H88.setImage(blueDot, for: .normal)
            
        default:
            print("Did not print the way")
            
        }
        
        counter = counter + 1
        
        print("second  counter\(counter), number of steps inside finish painting \(numberOfsteps)")
        
        
        if(counter == numberOfsteps){
            print("counter is equal to number of steps")
            guard timer != nil else { return }
            timer?.invalidate()
            timer = nil
            finalRouteFound = false
            
        }
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
struct goTo{
    var name: String!
    var selected: Bool!
    mutating func reset() {
        self = goTo()
    }
}
